<?php

Route::get('/', 'WelcomeController@index')->name('index');
Route::get('/aboutUs', 'WelcomeController@abouts')->name('about');
Route::get('/services/{id?}', 'WelcomeController@service')->name('service');

Route::get('/blog/news', 'WelcomeController@blogitems')->name('blognews');
Route::get('/blog/{id}', 'WelcomeController@blogid')->name('blogid');
Route::get('/contact', 'WelcomeController@contact')->name('contact');
Route::post('/send', 'WelcomeController@send')->name('send');

Route::redirect('/home', '/admin');

Auth::routes(['register' => false]);

Route::group(['prefix' => 'admin', 'as' => 'admin.', 'namespace' => 'Admin', 'middleware' => ['auth']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::resource('/contact', 'ContactController');

    
    Route::resource('users', 'UsersController');
    Route::resource('slider', 'SliderController');
    Route::resource('about', 'AboutParentController');
    Route::resource('aboutinform', 'AboutInformController');

    Route::delete('products/destroy', 'ProductsController@massDestroy')->name('products.massDestroy');

    Route::resource('products', 'ProductsController');
    Route::resource('blog', 'BlogController');
    Route::resource('service', 'ServiceController');
    Route::resource('service', 'ServiceController');
    Route::resource('service_info', 'ServiceInformateController');
    Route::resource('bcomments', 'BlogCommentsController');
    Route::post('bcomments/delete', 'BlogCommentsController@delete')->name('comment.delete');
});
