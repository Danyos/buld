<?php

namespace App\Http\Controllers;

use App\Models\AboutInformateModels;
use App\Models\AboutParentModels;
use App\Models\BlogModels;
use App\Models\ContactModel;
use App\Models\ServiceInformateModels;
use App\Models\ServiceModels;
use App\Models\SliderModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class WelcomeController extends Controller
{
    public function index()
    {
        $aboutParent=AboutParentModels::first();
        $aboutinformate=AboutInformateModels::orderBy('id','desc')->get();
        $blogItems=BlogModels::orderBy('id','desc')->get()->take(6);
        $services=ServiceModels::orderBy('id','desc')->get()->take(6);
        $slider=SliderModel::orderBy('id','desc')->get();
        return view('welcome',compact('blogItems','aboutParent','aboutinformate','services','slider'));
    }

    public function abouts()
    {

        $aboutParent=AboutParentModels::first();
        $aboutinformate=AboutInformateModels::orderBy('id','desc')->get();
        return view('page.about',compact('aboutParent','aboutinformate'));
    }

    public function service($id='')
    {

        if ($id){

            $lastservice=ServiceModels::find($id);
        }else{

            $lastservice=ServiceModels::first();
        }
        $id=$lastservice->id;

$service_informate=ServiceInformateModels::where('parent_id',$id)->get();




        return view('page.service',compact('lastservice','id','service_informate'));
    }

    public function blogitems()
    {
        $blogItems=BlogModels::orderBy('id','desc')->paginate(12);


        return view('page.blog.index',compact('blogItems'));
    }

    public function blogid($id)
    {
        $blogItems=BlogModels::find($id);

        return view('page.blog.show',compact('blogItems'));
    }

    public function contact()
    {

        return view('page.contact');
    }  public function send(Request  $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:120',
            'email' => 'required|email',
            'tel' => 'required|min:11|numeric',
            'message'=>'required'
        ]);
        if ( $validator->fails())
        {
            return back()->withErrors($validator)->withInput();
        }
       ContactModel::create($request->all());

        return back()->with('succsess','Your message in send');
    }
}
