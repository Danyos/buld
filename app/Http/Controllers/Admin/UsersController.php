<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\AdminControoler;
use App\Http\Requests\MassDestroyUserRequest;
use App\Http\Requests\StoreUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Role;
use App\User;
use Intervention\Image\Facades\Image;

class UsersController extends AdminControoler
{
    public function index()
    {
        abort_unless(\Gate::allows('user_access'), 403);

        $users = User::all();

        return view('admin.users.index', compact('users'));
    }

    public function create()
    {
        abort_unless(\Gate::allows('user_create'), 403);

        $roles = Role::all()->pluck('title', 'id');

        return view('admin.users.create', compact('roles'));
    }

    public function store(StoreUserRequest $request)
    {
        abort_unless(\Gate::allows('user_create'), 403);


        $user = User::create($request->all());
        $user->roles()->sync($request->input('roles', []));

        return redirect()->route('admin.users.index');
    }

    public function edit(User $user)
    {
        abort_unless(\Gate::allows('user_edit'), 403);

        $roles = Role::all()->pluck('title', 'id');

        $user->load('roles');

        return view('admin.users.edit', compact('roles', 'user'));
    }

    public function update(UpdateUserRequest $request, User $user)
    {
        abort_unless(\Gate::allows('user_edit'), 403);

        $user->update($request->all());
        if ($request->has('avatar')) {

            $imagename = date("Y") . '/' . date("m") . '/' . rand(1, 99999) . '.' . $request->avatar->getClientOriginalExtension();

            $destinationPath = public_path('assets/user/avatar');
            $pah = public_path('assets/user/avatar/' . date("Y") . '/' . date("m"));
            if (!file_exists($pah) > 0):
                if (!is_dir($pah . date("Y") . '/' . date("m"))) {
                    mkdir($pah, 0755, true);
                }endif;

            $thumb_img = Image::make($request->avatar->getRealPath())->encode('jpg', 75)->resize(650, 320);
            $thumb_img->save($destinationPath . '/' . $imagename, 80);
            $user->update([
                'avatar'=>'assets/user/avatar/'.$imagename
            ]);
        }
            $user->roles()->sync($request->input('roles', []));

        return redirect()->route('admin.home');
    }

    public function show(User $user)
    {
        abort_unless(\Gate::allows('user_show'), 403);

        $user->load('roles');

        return view('admin.users.show', compact('user'));
    }

    public function destroy(User $user)
    {
        abort_unless(\Gate::allows('user_delete'), 403);

        $user->delete();

        return back();
    }

    public function massDestroy(MassDestroyUserRequest $request)
    {
        User::whereIn('id', request('ids'))->delete();

        return response(null, 204);
    }
}
