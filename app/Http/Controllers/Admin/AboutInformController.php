<?php

namespace App\Http\Controllers\Admin;

use App\Models\AboutInformateModels;
use Illuminate\Http\Request;
use App\Http\Controllers\Admin\AdminControoler;

class AboutInformController extends AdminControoler
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $aboutinform=AboutInformateModels::get();
        return view('admin.about.informate.index',compact('aboutinform'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.about.informate.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        AboutInformateModels::create($request->all());
        return redirect()->route('admin.aboutinform.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $aboutinform=AboutInformateModels::find($id);
        return view('admin.about.informate.edit',compact('aboutinform','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        AboutInformateModels::find($id)->update($request->all());
        return redirect()->route('admin.aboutinform.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        AboutInformateModels::find($id)->delete();
        return redirect()->route('admin.aboutinform.index');
    }
}
