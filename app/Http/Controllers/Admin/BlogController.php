<?php

namespace App\Http\Controllers\Admin;

use App\Models\BlogCommentsModels;
use App\Models\BlogModels;
use Illuminate\Http\Request;
use App\Http\Controllers\Admin\AdminControoler;
use Intervention\Image\Facades\Image;

class BlogController extends AdminControoler
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $blog=BlogModels::get();

        return view('admin.blog.index', compact('blog'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.blog.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        if ($request->has('avatar')) {

            $imagename = date("Y") . '/' . date("m") . '/' . rand(1, 99999) . '.' . $request->avatar->getClientOriginalExtension();

            $destinationPath = public_path('blog/');
            $pah = public_path('blog/' . date("Y") . '/' . date("m"));
            if (!file_exists($pah) > 0):
                if (!is_dir($pah . date("Y") . '/' . date("m"))) {
                    mkdir($pah, 0755, true);
                }endif;

            $thumb_img = Image::make($request->avatar->getRealPath())->encode('jpg', 75);
            $thumb_img->save($destinationPath . '/' . $imagename, 80);

            BlogModels::create([
                'name'=>$request['name'],
                'info'=>$request['info'],
                'auth_offer'=>$request['auth_offer'],
                'auth'=>auth()->user()->id,
                'url'=>$request['url'],
                'avatar'=>'blog/'.$imagename,
            ]);

            return redirect()->route('admin.blog.index');
        }else{

            return back()->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $blog=BlogModels::find($id);
        $blogComments=BlogCommentsModels::where('blog_id',$id)->orderby('id','desc')->paginate(8);
        return view('admin.blog.show',compact('blog','blogComments'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $blog=BlogModels::find($id);
        return view('admin.blog.edit',compact('blog','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($request->has('avatar')) {

            $imagename = date("Y") . '/' . date("m") . '/' . rand(1, 99999) . '.' . $request->avatar->getClientOriginalExtension();

            $destinationPath = public_path('blog/');
            $pah = public_path('blog/' . date("Y") . '/' . date("m"));
            if (!file_exists($pah) > 0):
                if (!is_dir($pah . date("Y") . '/' . date("m"))) {
                    mkdir($pah, 0755, true);
                }endif;

            $thumb_img = Image::make($request->avatar->getRealPath())->encode('jpg', 75);
            $thumb_img->save($destinationPath . '/' . $imagename, 80);

            BlogModels::find($id)->update([
                'name'=>$request['name'],
                'info'=>$request['info'],
                'auth_offer'=>$request['auth_offer'],
                'auth'=>auth()->user()->id,
                'url'=>$request['url'],
                'avatar'=>'blog/'.$imagename,
            ]);

            return back();
        }else{
            BlogModels::find($id)->update([
                'auth_offer'=>$request['auth_offer'],
                'auth'=>auth()->user()->id,
                'name'=>$request['name'],
                'info'=>$request['info'],
                'url'=>$request['url'],

            ]);
            return back()->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        BlogModels::find($id)->delete();
        return back();
    }
}
