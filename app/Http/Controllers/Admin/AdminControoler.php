<?php

namespace App\Http\Controllers\Admin;

use App\Models\BlogModels;
use App\Models\ContactModel;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\View;

class AdminControoler extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct()
    {
        $contact = ContactModel::where('status','inactive')->get()->count();
        View::share('contact', $contact);
        $blog = BlogModels::get();
        View::share('blog', $blog);


    }
}


