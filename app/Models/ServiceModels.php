<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ServiceModels extends Model
{
    protected $fillable = [
        'title',
        'description',
        'offer',
        'avatar',
        'url'
    ];
}
