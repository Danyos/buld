<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BlogCommentsModels extends Model
{
    protected $fillable = [
        'user_id',
        'blog_id',
        'message',
        'avatar',
    ];
}
