<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class BlogModels extends Model
{
    protected $fillable = [
        'name',
        'avatar',
        'auth_offer',
        'auth',
        'url',
        'info',
    ];

    public function user(){
        return $this->hasOne(User::class,'id','auth');
    }
}
