@extends('admin.layouts.admin')

@section('content')

    <div class="card">


        <div class="card-body">
            <form action="{{ route("admin.slider.store") }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="form-group {{ $errors->has('url') ? 'has-error' : '' }}">
                    <label for="description">alt</label>
                    <br>
                    <input type="alt" name="alt" value="" class="form-control" required>
                    @if($errors->has('alt'))
                        <em class="invalid-feedback">
                            {{ $errors->first('alt') }}
                        </em>
                    @endif
                </div>


                <div class="form-group {{ $errors->has('avatar') ? 'has-error' : '' }}">
                    <label for="name">Фото*</label>
                    <input type="file" id="name" name="avatar" class="form-control" required>
                    @if($errors->has('avatar'))
                        <em class="invalid-feedback">
                            {{ $errors->first('avatar') }}
                        </em>
                    @endif
                </div>
                <div>
                    <input class="btn btn-danger" type="submit" value="Подтвердите">
                </div>
            </form>
        </div>
    </div>

@endsection

