@extends('admin.layouts.admin')
@section('content')


    <!-- Breadcrumb Area -->

    <div class="container-fluid">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('admin.home')}}">Главная</a></li>
                <li class="breadcrumb-item active" aria-current="page">Контакты</li>
            </ol>
        </nav>
    </div>

    <!-- Wrapper -->
    <div class="wrapper wrapper-content blog">
        <div class="container-fluid">
            <div class="row">

                @foreach($contact as $key => $contactd)
                <div class="col-md-6 col-lg-4">
                    <!-- Ibox -->
                    <div class="ibox bg-boxshadow mb-50">
                        <!-- Content -->
                        <div class="ibox-content blog">
                            <a >
                                <h4>{{ $contactd->name ?? '' }}</h4>
                            </a>
                            <!-- Date -->
                            <div class="blog-date">
                                <p><strong>{{$contactd->email}}</strong>
                                    <br>

                                </p>
                                <br>

                                <p><strong>{{$contactd->tel}}</strong> <span><i class="fa fa-clock-o"></i> <br>{{$contactd->created_at->format(' M d Y H:i')}}</span>
                                </p>
                            </div>
                            <p> {{$contactd->message}}</p>

                            <div class="row">
                                <div class="col-md-8">
                                    @can('contact_update')
                                    <h5 class="blog-tag">Настройка</h5>
                                    @if($contactd->status=='inactive')
                                    <button class="btn btn-white btn-xs blog" type="button" onclick="location.href='{{route('admin.contact.edit',$contactd->id)}}'">
                                        {{$contactd->status}}
                                    </button>
                                    @endif
                                    @endcan
                                        @can('contact_accses')

                                    <button class="btn btn-white btn-xs blog" type="button" onclick="location.href='{{route('admin.contact.show',$contactd->id)}}'">Посмотреть:</button>
                                        @endcan

                                    @can('about_delete')
                                        <form action="{{ route('admin.contact.destroy', $contactd->id) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                            <input type="hidden" name="_method" value="DELETE">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <button class="btn  btn-danger btn-xs blog" type="submit">Удалить</button>
                                        </form>
                                    @endcan
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                      @endforeach

            </div>
        </div>
    </div>


@endsection
