@extends('admin.layouts.admin')
@section('content')

    <div class="container-fluid">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('admin.home')}}">Главная</a></li>
                <li class="breadcrumb-item"><a href="{{route('admin.contact.index')}}">Контакты:</a></li>
                <li class="breadcrumb-item active" aria-current="page">{{$contact->name}}</li>
            </ol>
        </nav>
    </div>

<!-- Wrapper -->
<div class="wrapper wrapper-content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="product--item-arae bg-boxshadow">
                    <!-- Media -->
                    <div class="media align-items-center py-3 mb-4">
                        <div class="media-body ml-4">
                            <h4 class="mb-15">{{$contact->name}}</h4>
                            <a href="{{route('admin.contact.index')}}" class="btn btn-default btn-sm mb-15">Перейти к основному содержанию</a>
                        </div>
                    </div>

                    <!-- Nav Tabs -->
                    <div class="nav-tabs-top">
                        <ul class="nav nav-tabs">

                            <!-- Nav Item -->
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#item-images">Дополнительная информация:</a>
                            </li>
                        </ul>

                        <!-- Tab Content -->
                        <div class="tab-content">
                            <!-- Overview -->
                            <div class="tab-pane fade show active" id="item-overview">

                                <div class="card-body product-item-table">
                                    <h6 class="large font-weight-semibold mb-4">Информация</h6>
                                    <!-- Table -->
                                    <table class="table">
                                        <tbody>
                                        <tr>
                                            <td>Заглавие</td>
                                            <td>{{$contact->name}}</td>
                                        </tr>
                                        <tr>
                                            <td>почта:</td>
                                            <td>   {!! $contact->email !!}</td>
                                        </tr>
                                        <tr>
                                            <td>тел:</td>
                                            <td>   {!! $contact->tel !!}</td>
                                        </tr>
                                        <tr>
                                            <td>Описание</td>
                                            <td>   {!! $contact->message !!}</td>
                                        </tr>


                                        </tbody>
                                    </table>

                                </div>

                            </div>


                            <!-- Item Description -->

                            <!-- / Images -->

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
