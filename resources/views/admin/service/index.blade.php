@extends('admin.layouts.admin')
@section('content')


    <!-- Breadcrumb Area -->

    <div class="container-fluid">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('admin.home')}}">Главная</a></li>
                <li class="breadcrumb-item active" aria-current="page">Наши услуги</li>
            </ol>
        </nav>
    </div>
    @can('service_create')
    <div style="margin-bottom: 10px;" class="row">
        <div class="col-lg-12">
            <a class="btn btn-success" href="{{ route("admin.service.create") }}">
                Опубликовано
            </a>
        </div>
    </div>
    @endcan
    <!-- Wrapper -->
    <div class="wrapper wrapper-content blog">
        <div class="container-fluid">
            <div class="row">

                @foreach($service as $key => $services)
                <div class="col-md-6 col-lg-4">
                    <!-- Ibox -->
                    <div class="ibox bg-boxshadow mb-50">
                        <!-- Content -->
                        <div class="ibox-content blog">
                            <a href="">
                                <h4>{{ $services->title ?? '' }}</h4>
                            </a>
                            <!-- Date -->
                            <div class="blog-date">
                                <p><strong>{{$services->offer}}</strong> <span><i class="fa fa-clock-o"></i> {{$services->created_at->format(' M d Y H:i')}}</span>
                                </p>
                            </div>
                            <p> {{$services->description}}
                            </p>

                            <div class="row">
                                <div class="col-md-8">
                                    <h5 class="blog-tag">Настройка:</h5>
                                    @can('service_edit')
                                        <button class="btn btn-white btn-xs blog" type="button" onclick="location.href='{{route('admin.service.edit',$services->id)}}'">Редактировать</button>
                                    @endcan
                                    @can('service_show')
                                        <button class="btn btn-white btn-xs blog" type="button" onclick="location.href='{{route('admin.service.show',$services->id)}}'">Посмотреть</button>
                                    @endcan
                                    @can('service_create')
                                        <button class="btn btn-white btn-xs blog" type="button" onclick="location.href='{{route('admin.service_info.show',$services->id)}}'">Опубликовано</button>
                                    @endcan
                                    @can('service_delete')
                                        <form action="{{ route('admin.service.destroy', $services->id) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                            <input type="hidden" name="_method" value="DELETE">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <button class="btn  btn-danger btn-xs blog" type="submit">Удалить</button>
                                        </form>
                                    @endcan
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                      @endforeach

            </div>
        </div>
    </div>


@endsection
