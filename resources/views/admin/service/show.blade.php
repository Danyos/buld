@extends('admin.layouts.admin')
@section('content')

    <div class="card">
        <div class="card-header">
            Наши услуги
        </div>

        <div class="card-body">
            <table class="table table-bordered table-striped">
                <tbody>
                <tr>
                    <th>
                        Фото
                    </th>
                    <td>
                        <img src="{{asset($service->avatar)}}" alt="">
                    </td>
                </tr>
                <tr>
                    <th>
                        Заглавие
                    </th>
                    <td>
                        {{ $service->title }}
                    </td>
                </tr>
                <tr>
                    <th>
                        Описание
                    </th>
                    <td>
                        {{ $service->description }}
                    </td>
                </tr>
                <tr>
                    <th>
                        предложение авторизации пользователя
                    </th>
                    <td>
                        {{ $service->offer }}
                    </td>
                </tr>
                <tr>
                    <th>
                        Youtube Link
                    </th>
                    <td>
                        <iframe class="main-video-frame" src="{{ old('url', isset($service) ? $service->url : '') }}" frameborder="0" width="100%"
                                allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                allowfullscreen></iframe>
                    </td>
                </tr>
                </tbody>
            </table>
            <h1 align="center">Информировать</h1>
                <table class="table table-bordered table-striped">
                    <tbody>
                @foreach($service_informate as  $informate)
                <tr>

                        <th>
                            {{ $informate->title }}
                        </th>


                    <td>{{ $informate->description }} </td>
                    <td>  <button class="btn btn-white btn-xs blog" type="button" onclick="location.href='{{route('admin.service_info.edit',$informate->id)}}'">
                            Редактировать</button>
                    </td>
                    <td>   @can('product_delete')
                            <form action="{{ route('admin.service_info.destroy', $informate->id) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                <input type="hidden" name="_method" value="DELETE">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <button class="btn  btn-danger btn-xs blog" type="submit">Удалить</button>
                            </form>
                        @endcan</td>

                </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

@endsection
