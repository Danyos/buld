@extends('admin.layouts.admin')

@section('content')

    <div class="card">


        <div class="card-body">
            <form action="{{ route("admin.service_info.store") }}" method="POST" enctype="multipart/form-data">
                @csrf


                <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                    <label for="name">Заглавие</label>
                    <input type="text" id="name" name="title" class="form-control" value="" required>
                    @if($errors->has('title'))
                        <em class="invalid-feedback">
                            {{ $errors->first('title') }}
                        </em>
                    @endif
                </div>


                <div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
                    <label for="description">Описание </label>
                    <textarea id="description" name="description" class="form-control inputFields ncontent" required></textarea>
                    @if($errors->has('description'))
                        <em class="invalid-feedback">
                            {{ $errors->first('description') }}
                        </em>
                    @endif
                </div>
                <input type="hidden" name="parent_id" value="{{$id}}">

                <div>
                    <input class="btn btn-danger" type="submit" value="Подтвердите">
                </div>
            </form>
        </div>
    </div>

@endsection

