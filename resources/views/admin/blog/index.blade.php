@extends('admin.layouts.admin')
@section('content')


    <!-- Breadcrumb Area -->

    <div class="container-fluid">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('admin.home')}}">Главная</a></li>
                <li class="breadcrumb-item active" aria-current="page">Блог</li>
            </ol>
        </nav>
    </div>

    <div style="margin-bottom: 10px;" class="row">
        <div class="col-lg-12">
            @can('news_create')
            <a class="btn btn-success" href="{{ route("admin.blog.create") }}">
                Опубликовано
            </a>
            @endcan
        </div>
    </div>
    <!-- Wrapper -->
    <div class="wrapper wrapper-content blog">
        <div class="container-fluid">
            <div class="row">

                @foreach($blog as $key => $bloger)
                <div class="col-md-6 col-lg-4">
                    <!-- Ibox -->
                    <div class="ibox bg-boxshadow mb-50">
                        <!-- Content -->
                        <div class="ibox-content blog">
                            <a >
                                <h4>{{ $bloger->name ?? '' }}</h4>
                            </a>
                            <!-- Date -->
                            <div class="blog-date">
                                <p><strong>{{$bloger->user->name}}</strong> <span><i class="fa fa-clock-o"></i> {{$bloger->created_at->format(' M d Y H:i')}}</span>
                                </p>
                            </div>
                            <p>    {{ mb_strlen( strip_tags($bloger->info, '<b><i><u>') ) > 300 ? mb_substr(strip_tags($bloger->info, '<b><i><u>'), 0, 300) . ' ...' : strip_tags($bloger->info, '<b><i><u>') }}
                            </p>

                            <div class="row">
                                <div class="col-md-8">
                                    <h5 class="blog-tag">Настройка:</h5>
                                    @can('news_edit')
                                    <button class="btn btn-white btn-xs blog" type="button" onclick="location.href='{{route('admin.blog.edit',$bloger->id)}}'">Редактировать</button>
                                    @endcan
                                    @can('news_delete')
                                        <form action="{{ route('admin.blog.destroy', $bloger->id) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                            <input type="hidden" name="_method" value="DELETE">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <button class="btn  btn-danger btn-xs blog" type="submit">Удалить</button>
                                        </form>
                                    @endcan
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                      @endforeach

            </div>
        </div>
    </div>


@endsection
