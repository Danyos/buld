@extends('admin.layouts.admin')
@section('content')

    <div class="container-fluid">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('admin.home')}}">Գլխավո</a></li>
                <li class="breadcrumb-item"><a href="{{route('admin.blog.index')}}">Բլոգ</a></li>
                <li class="breadcrumb-item active" aria-current="page">{{$blog->name_am}}</li>
            </ol>
        </nav>
    </div>

<!-- Wrapper -->
<div class="wrapper wrapper-content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="product--item-arae bg-boxshadow">
                    <!-- Media -->
                    <div class="media align-items-center py-3 mb-4">
                        <img src="{{asset($blog->avatar)}}" alt="" class="product-thumb d-block">
                        <!-- Media Body -->
                        <div class="media-body ml-4">
                            <h4 class="mb-15">{{$blog->name_am}}</h4>
                            <a href="{{route('admin.blog.edit',$blog->id)}}" class="btn btn-primary btn-sm mb-15">Փոփոխել</a>&nbsp;
                            <a href="{{route('admin.blog.index')}}" class="btn btn-default btn-sm mb-15">Անցնել գլխավոր բլոգներ</a>
                        </div>
                    </div>

                    <!-- Nav Tabs -->
                    <div class="nav-tabs-top">
                        <ul class="nav nav-tabs">
                            <!-- Nav Item -->
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#item-overview">Հայերեն</a>
                            </li>
                            <!-- Nav Item -->
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#item-description">Ռուսերեն</a>
                            </li>
                            <!-- Nav Item -->
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#item-discounts">Անգլերեն</a>
                            </li>
                            <!-- Nav Item -->
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#item-images">Հավելյար ինֆորմացյա</a>
                            </li>  <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#item-Comments">Comments</a>
                            </li>
                        </ul>

                        <!-- Tab Content -->
                        <div class="tab-content">
                            <!-- Overview -->
                            <div class="tab-pane fade show active" id="item-overview">

                                <div class="card-body product-item-table">
                                    <h6 class="large font-weight-semibold mb-4">Ինֆօրմացյա</h6>
                                    <!-- Table -->
                                    <table class="table">
                                        <tbody>
                                        <tr>
                                            <td>Վերնագիր:</td>
                                            <td>{{$blog->name_am}}</td>
                                        </tr>
                                        <tr>
                                            <td>Նկարագրություն:</td>
                                            <td>   {!! $blog->info_am !!}</td>
                                        </tr>


                                        </tbody>
                                    </table>

                                </div>

                            </div>


                            <!-- Item Description -->
                            <div class="tab-pane fade" id="item-description">
                                <div class="card-body p-3">

                                    <div class="row">

                                        <h6 class="large font-weight-semibold mb-4">Ինֆօրմացյա</h6>
                                        <!-- Table -->
                                        <table class="table">
                                            <tbody>
                                            <tr>
                                                <td>Վերնագիր:</td>
                                                <td>{{$blog->name_ru}}</td>
                                            </tr>
                                            <tr>
                                                <td>Նկարագրություն:</td>
                                                <td>   {!! $blog->info_ru !!}</td>
                                            </tr>


                                            </tbody>
                                        </table>

                                    </div>

                                </div>
                            </div>

                            <!-- Item Discounts -->
                            <div class="tab-pane fade" id="item-discounts">
                                <div class="card-body">
                                    <!-- Table Responsive -->
                                    <div class="table-responsive">
                                        <h6 class="large font-weight-semibold mb-4">Ինֆօրմացյա</h6>
                                        <!-- Table -->
                                        <table class="table">
                                            <tbody>
                                            <tr>
                                                <td>Վերնագիր:</td>
                                                <td>{{$blog->name_en}}</td>
                                            </tr>
                                            <tr>
                                                <td>Նկարագրություն:</td>
                                                <td>   {!! $blog->info_en !!}</td>
                                            </tr>


                                            </tbody>
                                        </table>
                                    </div>

                                </div>
                            </div>
                            <!-- / Discounts -->

                            <!-- Images -->
                            <div class="tab-pane fade" id="item-images">
                                <div class="card-body">

                                    <div class="mb-4">
                                        <span class="badge badge-dot badge-primary"></span> Primary image
                                    </div>

                                    <!-- Lightbox template -->

                                    <!-- Item Image -->
                                    <div id="product-item-images" class="row">
                                        <div class="col-12 col-sm-6 col-md-4 col-xl-3 mb-4">
                                            <a href="{{asset($blog->avatar)}}" class="d-block"><img src="{{asset($blog->avatar)}}" class="img-fluid" alt=""></a>
                                        </div>
                                        <div class="col-12 col-sm-6 col-md-8 col-xl-9 mb-8">
                                            <iframe width="1200" src="{{$blog->url}}" frameborder="0" allowfullscreen  >

                                            </iframe>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="item-Comments">
                                <div class="card-body">

                                    <div class="mb-4">
                                        <span class="badge badge-dot badge-primary"></span> Primary Comments
                                    </div>

                                    <!-- Lightbox template -->

                                    <!-- Item Image -->
                                    <div id="product-item-images" class="row">
                                        <div class="row">
                                            <!-- Comments -->
                                            <div class="col-lg-12">
                                                <div class="articles-heading mt-30">
                                                    <h2>Comments:</h2>
                                                    {{$blogComments->links()}}
                                                </div>

<div class="oks col-lg-12">


            @foreach($blogComments->reverse() as $comments)
            <div class="article-social-feed-box id-{{$comments->id}}">
                <!-- Social Avatar -->
                <div class="social-avatar">
                    <a href="#" class="social-image"><img src="img/thumbnails-img/blog-1.jpg" alt=""></a>
                    <!-- Media Body -->
                    <div class="media-body">
                        <a href="#">Andrew Williams,</a>
                        <small>Today 4:21 pm - 12.06.2018</small>



                        <input type="hidden" name="id" id="comments_id" value="{{$comments->id}}">
                        <button class="btn  btn-danger btn-xs blog" type="button" onclick="deletecommens()">Ջնջել</button>

                    </div>
                </div>
                <!-- Social Body -->
                <div class="social-body">
                    <p>{{$comments->message}}</p>
                </div>
            </div>

@endforeach
</div>



                                                <form action="javascript::void(0)" method="POST" id="ffre">
                                                    <input type="hidden" name="_method" value="DELETE">
                                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                </form>
                                                <div class="form-group {{ $errors->has('message') ? 'has-error' : '' }}">
                                                    <label for="description">Նկարագրութհուն</label>
                                                    <textarea id="description" name="message" class="form-control" style="border: 1px solid black;">{{ old('message') }}</textarea>
                                                    <input type="hidden" name="blog_id" id="blog_id" value="{{$blog->id}}">

                                                </div>
                                                <div>
                                                    <input class="btn btn-danger jutak" type="button" value="Հաստատել" onclick="getUserAccount()">
                                                </div>

                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <!-- / Images -->

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>

    <script>
        function getUserAccount() {
            var msg=document.getElementById('description').value;
            var blog=document.getElementById('blog_id').value;
    axios.post('{{route('admin.bcomments.store')}}',{'msg': msg,'blog':blog})
        .then(function (response) {

            $('.oks').append(response.data)
        })
}
function deletecommens() {

            var id=document.getElementById('comments_id').value;

    axios.post('{{route('admin.comment.delete')}}',{'id': id})
        .then(function (response) {
console.log(response.data);
            $('.id-'+response.data).remove().hide();
        })
}

    </script>
@endsection
