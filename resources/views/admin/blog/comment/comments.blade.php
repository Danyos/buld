<div class="article-social-feed-box">
    <!-- Social Avatar -->
    <div class="social-avatar">
        <a href="#" class="social-image"><img src="img/thumbnails-img/blog-1.jpg" alt=""></a>
        <!-- Media Body -->
        <div class="media-body">
            <a href="#">Andrew Williams,</a>
            <small>Today 4:21 pm - 12.06.2018</small>
        </div>
    </div>
    <!-- Social Body -->
    <div class="social-body">
        <p>{{$blog->message}}</p> </div>
</div>
