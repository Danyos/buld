<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from demo.designing-world.com/appwork-2.1.0/default-version/dashboard-1.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 30 Jul 2019 12:01:48 GMT -->
<head>
    <meta charset="UTF-8">
    <meta name="description" content="Colors">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- The above 4 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <!-- Title -->
    <title>AdminPage</title>
    <link href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" rel="stylesheet" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" rel="stylesheet" />
    <!-- Favicon -->
    <link rel="icon" href="{{asset('asset/image/logo.png')}}">

    <!-- Footable Stylesheet -->
    <link rel="stylesheet" href="{{asset('assets/css/plugins/products-css/footable.css')}}">
@yield('css')
    <!-- Core Stylesheet -->
    <link rel="stylesheet" href="{{asset('assets/style.css')}}">

    <!-- Responsive Stylesheet -->
    <link rel="stylesheet" href="{{asset('assets/css/responsive.css')}}">

</head>

<body>
<div class="page-wrapper">

    <!-- Page Top Bar Area -->
    <div class="page-top-bar-area d-flex align-items-center justify-content-between">

        <!-- Logo Trigger Area -->
        <div class="logo-trigger-area d-flex align-items-center">

            <!-- logo -->
            <a href="{{route('admin.home')}}" class="logo">
                    <span class="big-logo">
                        <img src="{{asset('asset/image/logo.png')}}" alt="">
                    </span>
                <span class="small-logo">
                        <img src="{{asset('asset/image/unlogoNew.png')}}" alt="">
                    </span>
            </a>

            <!-- Trigger -->
            <div class="top-trigger">
                <span></span>
                <span></span>
                <span></span>
            </div>
        </div>

        <!-- Top Search Bar -->
        <div class="top-search-bar">
{{--            <form action="#" method="get">--}}
{{--                <input type="search" name="search" class="top-search" placeholder="Search...">--}}
{{--                <button type="submit"><i class="pe-7s-search"></i></button>--}}
{{--            </form>--}}
        </div>

        <!-- User Meta -->
        <div class="user-meta-data d-flex align-items-center">

            <!-- Language -->

            <!-- Profile -->
            <div class="topbar-profile">
                <!-- Thumb -->
                <div class="user---thumb">
                    <img src="{{asset(\Illuminate\Support\Facades\Auth::user()->avatar)}}" alt="">
                </div>
                <!-- Profile Data -->
                <div class="profile-data">
                    <!-- Profile User Details -->
                    <div class="profile-user--details" style="background-image: url(img/thumbnails-img/profile-bg.jpg);">
                        <!-- Thumb -->
                        <div class="user---thumb">
                            <img src="{{asset(\Illuminate\Support\Facades\Auth::user()->avatar)}}" style="ob" alt="">
                        </div>
                        <!-- Profile Text -->
                        <div class="profile---text-details">
                            <h6 style="color: black">{{\Illuminate\Support\Facades\Auth::user()->name}}</h6>
                            <a  style="color: black">{{\Illuminate\Support\Facades\Auth::user()->email}}</a>
                        </div>
                    </div>
                    <!-- Profile List Data -->
                    <a class="profile-list--data" href="#">
                        <!-- Profile icon -->
                        <div class="profile--list-icon">
                            <i class="fa fa-user" aria-hidden="true"></i>
                        </div>
                        <!-- Profile Text -->
                        <div class="notification--list-body-text profile" onclick="location.href='{{ route('admin.users.show', \Illuminate\Support\Facades\Auth::user()->id) }}'">
                            <h6>Мой профайл</h6>
                        </div>
                    </a>

                    <!-- Profile List Data -->
                    <a class="profile-list--data" href="#">
                        <!-- Profile icon -->
                        <div class="profile--list-icon">
                            <i class="fa fa-cog" aria-hidden="true"></i>
                        </div>
                        <!-- Profile Text -->
                        <div class="notification--list-body-text profile" onclick="location.href='{{ route('admin.users.edit', \Illuminate\Support\Facades\Auth::user()->id) }}'">
                            <h6>Настройки учетной записи</h6>
                        </div>
                    </a>
                    <!-- Profile List Data -->

                    <!-- Profile List Data -->
                    <a class="profile-list--data" href="#">
                        <!-- Profile icon -->
                        <div class="profile--list-icon">
                            <i class="fa fa-sign-out text-danger" aria-hidden="true"></i>
                        </div>
                        <!-- Profile Text -->
                        <div class="notification--list-body-text profile" onclick="$('#logoutform').submit()">
                            <h6>Выход</h6>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>

    <!-- ###### Layout Container Area ###### -->
    <div class="layout-container-area mt-70">
        <!-- Side Menu Area -->
    @include('admin.partials.menu')

        <!-- Layout Container -->
        <div class="layout-container sidemenu-container mt-100">
@yield('content')
        </div>
    </div>
</div>

<form id="logoutform" action="{{ route('logout') }}" method="POST" style="display: none;">
    {{ csrf_field() }}
</form>

<!-- jQuery 2.2.4 -->

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="{{asset('assets2/js/bootstrap.min.js')}}"></script>
<!-- Bootsrap js -->
<script src="{{asset('assets/assets/js/bootstrap/popper.min.js')}}"></script>


<!-- Plugins js -->
<script src="{{asset('assets/js/plugins-js/chart-js/chart.min.js')}}"></script>
<script src="{{asset('assets/js/plugins-js/chart-js/chart-demo.js')}}"></script>

<script src="{{asset('assets/js/plugins-js/classy-nav.js')}}"></script>

<!-- Footable js -->
<script src="{{asset('assets/js/plugins-js/product-list-js/footable.js')}}"></script>

<!-- Active js -->
<script src="{{asset('assets/js/active.js')}}"></script>
<script  src="{{asset('assets2/tinymce/tinymce.min.js')}}"></script>
@yield('js')
</body>
</html>
