@extends('admin.layouts.admin')

@section('content')
    @can('about_edit')
    <div class="card">
        <div class="card-header">
            редактировать:  О нас
        </div>

        <div class="card-body">
            <form action="{{ route("admin.about.update",1) }}" method="POST" enctype="multipart/form-data">
                @csrf
                @method('put')

                <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                    <label for="title">Заглавие:</label>
                    <input type="text" id="title" name="title" class="form-control" value="{{ old('title', isset($about) ? $about->title : '') }}" required>
                    @if($errors->has('title'))
                        <em class="invalid-feedback">
                            {{ $errors->first('title') }}
                        </em>
                    @endif
                </div>


                <div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
                    <label for="description">Описание: </label>
                    <textarea id="description" name="description" class="form-control" required>{{ old('description', isset($about) ? $about->description : '') }}</textarea>
                    @if($errors->has('description'))
                        <em class="invalid-feedback">
                            {{ $errors->first('description') }}
                        </em>
                    @endif
                </div>
                <div class="form-group {{ $errors->has('avatar') ? 'has-error' : '' }}">
                    <label for="name">Фото:*</label>
                    <input type="file" id="name" name="avatar" class="form-control" required>
                    @if($errors->has('avatar'))
                        <em class="invalid-feedback">
                            {{ $errors->first('avatar') }}
                        </em>
                    @endif
                </div>
                <div class="form-group {{ $errors->has('url') ? 'has-error' : '' }}">
                    <label for="description">Youtube Link</label>
                    <br>
                    <input type="url" name="url" value="{{ old('url', isset($about) ? $about->url : '') }}" class="form-control">
                    @if($errors->has('url'))
                        <em class="invalid-feedback">
                            {{ $errors->first('url') }}
                        </em>
                    @endif
                </div>
                <div>
                    <input class="btn btn-danger" type="submit" value="Հաստատել">
                </div>
            </form>
        </div>
        <iframe class="main-video-frame" src="{{ old('url', isset($about) ? $about->url : '') }}" frameborder="0"
                allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                allowfullscreen></iframe>
        <img src="{{ old('url', isset($about) ?asset($about->avatar)  : '') }}" alt="" width="300px">
    </div>
    @endcan
@endsection

