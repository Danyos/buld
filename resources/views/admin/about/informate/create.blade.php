@extends('admin.layouts.admin')

@section('content')

    <div class="card">
        <div class="card-header">
            {{ trans('global.create') }}
        </div>

        <div class="card-body">
            <form action="{{ route("admin.aboutinform.store") }}" method="POST" enctype="multipart/form-data">
                @csrf



                <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                    <label for="title">Заглавие</label>
                    <input type="text" id="title" name="title" class="form-control" value="{{ old('title', isset($aboutinform) ? $aboutinform->name : '') }}" required>
                    @if($errors->has('title'))
                        <em class="invalid-feedback">
                            {{ $errors->first('title') }}
                        </em>
                    @endif
                </div>


                <div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
                    <label for="description">Описание </label>
                    <textarea id="description" name="description" class="form-control" required>{{ old('description', isset($aboutinform) ? $aboutinform->description : '') }}</textarea>
                    @if($errors->has('description'))
                        <em class="invalid-feedback">
                            {{ $errors->first('description') }}
                        </em>
                    @endif
                </div>

                <div>
                    <input class="btn btn-danger" type="submit" value="Подтвердите">
                </div>
            </form>
        </div>
    </div>

@endsection

