@extends('admin.layouts.admin')

@section('content')

    <div class="card">
        <div class="card-header">
            Редактировать:
        </div>

        <div class="card-body">
            <form action="{{ route("admin.aboutinform.update",$id) }}" method="POST" enctype="multipart/form-data">
                @csrf
                @method('put')

                <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                    <label for="title">Заглавие</label>
                    <input type="text" id="title" name="title" class="form-control" value="{{ old('title', isset($aboutinform) ? $aboutinform->title : '') }}" required>
                    @if($errors->has('title'))
                        <em class="invalid-feedback">
                            {{ $errors->first('title') }}
                        </em>
                    @endif
                </div>


                <div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
                    <label for="description">Описание </label>
                    <textarea id="description" name="description" class="form-control" required>{{ old('description', isset($aboutinform) ? $aboutinform->description : '') }}</textarea>
                    @if($errors->has('description'))
                        <em class="invalid-feedback">
                            {{ $errors->first('description') }}
                        </em>
                    @endif
                </div>
                <div>
                    <input class="btn btn-danger" type="submit" value="Подтвердите">
                </div>
            </form>
        </div>
    </div>
@endsection

