@extends('admin.layouts.admin')
@section('content')


    <!-- Breadcrumb Area -->

    <div class="container-fluid">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('admin.home')}}">Главная</a></li>
                <li class="breadcrumb-item active" aria-current="page">О нас </li>
            </ol>
        </nav>
    </div>
    @can('about_create')
    <div style="margin-bottom: 10px;" class="row">
        <div class="col-lg-12">
            <a class="btn btn-success" href="{{ route("admin.aboutinform.create") }}">
                Опубликовано
            </a>
        </div>
    </div>
    @endcan
    <!-- Wrapper -->
    <div class="wrapper wrapper-content blog">
        <div class="container-fluid">
            <div class="row">

                @foreach($aboutinform as $key => $inform)
                    <div class="col-md-6 col-lg-4">
                        <!-- Ibox -->
                        <div class="ibox bg-boxshadow mb-50">
                            <!-- Content -->
                            <div class="ibox-content blog">
                                <a href="">
                                    <h4>{{ $inform->title ?? '' }}</h4>
                                </a>
                                <p>  {{ $inform->description ?? '' }}   </p>

                                <div class="row">
                                    <div class="col-md-8">
                                        <h5 class="blog-tag">Настройка:</h5>

                                        @can('about_edit')
                                        <button class="btn btn-white btn-xs blog" type="button" onclick="location.href='{{route('admin.aboutinform.edit',$inform->id)}}'">Редактировать</button>
                                        @endcan

                                        @can('about_delete')
                                            <form action="{{ route('admin.aboutinform.destroy', $inform->id) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                                <input type="hidden" name="_method" value="DELETE">
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                <button class="btn  btn-danger btn-xs blog" type="submit">Удалить</button>
                                            </form>
                                        @endcan
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                @endforeach

            </div>
        </div>
    </div>



@endsection
