@extends('layouts.app')
@section('content')
    <div class="mSLide">
        @foreach($slider as $sliders)
        <div>
            <div class="mSlide-window">
            </div>
            <img src="{{asset($sliders->avatar)}}" alt="{{$sliders->alt}}">
        </div>
        @endforeach
    </div>

    <div class="container main-info">

        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <h2 class="main-info-header-undoText">О нас</h2>
                <p class="main-info-header-text"><b></b></p>
            </div>
        </div>
        <div class="row">

            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <p class="main-info-header-text" style="border-bottom: none;"><b>{{$aboutParent->title}}</b></p>
            </div>
            <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-5">

                <div class="main-info-windows">

                    <div class="mi_first-flex">
                        @foreach($aboutinformate as $inform)
                        <div>
                            <h5 class="text-left">{{$inform->title}}</h5>
                            <p>
                                {{$inform->description}}
                            </p>
                        </div>
                        @endforeach
                    </div>

                </div>
            </div>
            <div class="col-12 col-sm-12 col-md-12 col-lg-8 col-xl-7">
                <div class="main-info-windows">
                    <img src="{{asset($aboutParent->avatar)}}" alt="{{$aboutParent->title}}">
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid bg-light main-window">
        <div class="container main-video-window mvw-1">

            <div class="mvw-text">
                <div>
                    <p class="text-left">
                        {{$aboutParent->description}}
                    </p>
                </div>
                @if($aboutParent->url)
                <div>

                    <iframe class="main-video-frame" src="{{$aboutParent->url}}" frameborder="0"
                            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                            allowfullscreen></iframe>

                </div>
                    @endif
            </div>
        </div>
    </div>

    <div class="product_show container">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <h2 class="main-info-header-undoText">Наши услуги</h2>
                <p class="main-info-header-text"><b></b></p>
            </div>
        </div>
        <div class="row">
            @foreach($services as $serviceMain)
            <div class="col-12 col-sm-6 col-md-4 col-xl-4">
                <div class="psh-window">
                    <div class="psh-img">
                        <div class="_hover">
                            <div class="_hover-window">
                                <a href="{{route('service',$serviceMain->id)}}"><i class="fa fa-link" aria-hidden="true"></i></a>
                            </div>
                        </div>
                        <img src="{{asset($serviceMain->avatar)}}" alt="{{$serviceMain->title}}" style="height: 270px; object-fit: cover;">
                    </div>
                    <div class="psh-text">
                        <h3>{{$serviceMain->title}}</h3>
                        <p>
                            {{ mb_strlen( strip_tags($serviceMain->description, '<b><i><u>') ) > 100 ? mb_substr(strip_tags($serviceMain->description, '<b><i><u>'), 0, 100) . ' ...' : strip_tags($serviceMain->description, '<b><i><u>') }}
                        </p>
                        </p>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>

    <div class="container opw_main-window">

        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <h2 class="main-info-header-undoText">Блог</h2>
                <p class="main-info-header-text"><b></b></p>
            </div>
        </div>
        <div class="row">
            @foreach($blogItems as $blogItem)
            <div class="col-12 col-sm-6 col-md-6 col-lg-4 col-xl-4">
                <div class="oder-product-window" onclick="location.href='{{route('blogid',$blogItem->id)}}'">
                    <div class="opw-text">
                        <h3 class="text-center">{{$blogItem->name}}</h3>
                        <p>
                            {{ mb_strlen( strip_tags($blogItem->info, '<b><i><u>') ) > 200 ? mb_substr(strip_tags($blogItem->info, '<b><i><u>'), 0, 200) . ' ...' : strip_tags($blogItem->info, '<b><i><u>') }}
                        </p>
                        <div class="opw-go text-center"></div>
                        <div class="opw-arrow">
                            <a href="javascript: void (0);"><i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                        </div>
                    </div>
                    <div class="opw-image">
                        <img src="{{asset($blogItem->avatar)}}" alt="{{$blogItem->name}}"  style="height: 270px; object-fit: cover;">
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>

    <div class="container-fluid sMes">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <h2 class="main-info-header-undoText">Контакты</h2>
                <p class="main-info-header-text"><b></b></p>
            </div>
        </div>
        <form class="container" action="{{route('send')}}" method="post" id="myLocation">
            @csrf
            @if(session()->has('succsess'))
                <span class="help-block">
                        <p class="alert alert-success">
                <strong>Внимание!</strong> ваш запрос был отправлен.
                        </p>
                       </span>
            @endif
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <p class="main-info-header-text" style="border-bottom: none;"><b>Отправьте свои вопросы</b></p>
            </div>
            <div class="how_you">
                <div>
                    <label for="_name">Имя <span class="red">*</span></label>
                    <input type="text" id="_name"  class="form-control" placeholder="John" name="name" value="{{old('name')}}" required>
                    @if ($errors->has('name'))
                        <br>
                        <span class="help-block">
                        <p class="alert alert-danger">
                <strong>Внимание!</strong> Заполните поле имени.
                        </p>
                       </span>
                    @endif
                </div>
                <div>
                    <label for="_email">Эл. адрес <span class="red">*</span></label>
                    <input type="email" id="_email" class="form-control" placeholder="example@gmail.com" name="email" value="{{old('email')}}" required>
                    @if ($errors->has('email'))
                        <br>
                        <span class="help-block">
                        <p class="alert alert-danger">
                <strong>Внимание!</strong> Заполните поле Электронная почта.
                        </p>
                       </span>
                    @endif
                </div>
            </div>
            <div class="how_subj">
                <label for="_subject">Телефон <span class="red">*</span></label>
                <input type="text" id="_subject" class="form-control" placeholder="Telephone" name="tel" value="{{old('tel')}}" required>
                @if ($errors->has('tel'))
                    <br>
                    <span class="help-block">
                        <p class="alert alert-danger">
                <strong>Внимание!</strong>Заполните поле телефона.
                        </p>
                       </span>
                @endif
            </div>
            <div class="how_subj">
                <label for="_message">Сообщение <span class="red">*</span></label>
                <textarea  id="_message" class="form-control" placeholder="Сообщение" name="message" >{{old('message')}}</textarea>
                @if ($errors->has('message'))
                    <br>
                    <span class="help-block">
                        <p class="alert alert-danger">
                <strong>Внимание!</strong> Заполните поле сообщения.
                        </p>
                       </span>
                @endif
            </div>
            <div class="text-right">
                <button type="submit" class="mySendBtn">ОТПРАВИТЬ</button>
            </div>
        </form>
    </div>

@endsection
