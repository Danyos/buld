@extends('layouts.app')
@section('content')
    <div class="container-fluid page_title" style="background-image: url('image/ind_2.png')!important;">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h5 class="page_tittle" >О нас</h5>
                    <div class="bread_crumb text-lg-left">
                        <a href="{{route('index')}}">Главная<i class="fa fa-angle-right pl-2" aria-hidden="true"></i></a>
                        <span class="activeColor">О нас</span>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="container main-info">

        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">

                <p class="main-info-header-text" style="border-bottom: none;"><b>{{$aboutParent->title}}</b></p>
            </div>
        </div>
        <div class="row">


            <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-5">

                <div class="main-info-windows">

                    <div class="mi_first-flex">
                        @foreach($aboutinformate as $inform)
                            <div>
                                <h5 class="text-left">{{$inform->title}}</h5>
                                <p>
                                    {{$inform->description}}
                                </p>
                            </div>
                        @endforeach
                    </div>

                </div>
            </div>
            <div class="col-12 col-sm-12 col-md-12 col-lg-8 col-xl-7">
                <div class="main-info-windows">
                    <img src="{{asset($aboutParent->avatar)}}" alt="{{$inform->title}}">
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid bg-light main-window">
        <div class="container main-video-window mvw-1">

            <div class="mvw-text">
                <div>
                    <p class="text-left">
                        {{$aboutParent->description}}
                    </p>
                </div>
                @if($aboutParent->url)
                    <div>

                        <iframe class="main-video-frame" src="{{$aboutParent->url}}" frameborder="0"
                                allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                allowfullscreen></iframe>

                    </div>
                @endif
            </div>
        </div>
    </div>


    <div class="container-fluid pv_paralax">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="text-center pv_paralax_text">
                        <h2>25+ years of experiences for give you better results.</h2>
                        <p>
                            There are many variations of passages of Lorem Ipsum but
                            majority have suffered alteration form by injected humour or randomised words.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection
