@extends('layouts.app')
@section('content')


    <div class="container-fluid page_title" style="background-image: url('image/ind_2.png')!important;">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h5 class="page_tittle">Связаться с нами</h5>
                    <div class="bread_crumb text-lg-left">
                        <a href="{{route('index')}}">Главная<i class="fa fa-angle-right pl-2"
                                                               aria-hidden="true"></i></a>
                        <span class="activeColor">Контакт</span>
                    </div>
                </div>
            </div>
        </div>
    </div>

{{--    succsess--}}

        <br>
    @if(session()->has('succsess'))
        <span class="help-block">
                        <p class="alert alert-success">
                <strong>Внимание!</strong> ваш запрос был отправлен.
                        </p>
                       </span>
@endif
    <div>
        <div class="container">
            <div class="contact-parent">
                <div class="contact-child child1">
                    <p>
                        <i class="fa fa-map-marker-alt"></i> Адрес <br/>
                        <span> Ash Lane 110
                                <br/>
                                London, UK
                            </span>
                    </p>

                    <p>
                        <i class="fa fa-phone-alt"></i> Давайте поговорим<br/>
                        <span> 0787878787</span>
                    </p>

                    <p>
                        <i class=" fa fa-envelope"></i> Общая поддержка<br/>
                        <span>andreea@andreeabunget.co.uk</span>
                    </p>
                </div>

                <div class="contact-child child2">
                    <form class="inside-contact" action="{{route('send')}}" method="post">
                        @csrf
                        <h2>Связаться с нами</h2>
                        <h3>
                               <span id="confirm">
                        </h3>

                        <p>Имя *</p>
                        <input id="txt_name" type="text" Required="required" name="name" value="{{old('name')}}">

                        @if ($errors->has('name'))
                            <br>
                            <span class="help-block">
                        <p class="alert alert-danger">
                <strong>Внимание!</strong> Заполните поле имени.
                        </p>
                       </span>
                        @endif

                        <p>Эл. адрес *</p>
                        <input id="txt_email" type="email" Required="required" name="email" value="{{old('email')}}">
                        @if ($errors->has('email'))
                            <br>
                            <span class="help-block">
                        <p class="alert alert-danger">
                <strong>Внимание!</strong> Заполните поле Электронная почта.
                        </p>
                       </span>
                        @endif

                        <p>Телефон *</p>
                        <input id="txt_phone" type="text" Required="required" name="tel" value="{{old('tel')}}" >
                        @if ($errors->has('tel'))
                        <br>
                        <span class="help-block">
                        <p class="alert alert-danger">
                <strong>Внимание!</strong>Заполните поле телефона.
                        </p>
                       </span>
                        @endif

                        <p>Сообщение *</p>
                        <textarea id="txt_message" rows="4" cols="20" Required="required"
                                  name="message">{{old('message')}}</textarea>
                        @if ($errors->has('message'))
                            <br>
                            <span class="help-block">
                        <p class="alert alert-danger">
                <strong>Внимание!</strong> Заполните поле сообщения.
                        </p>
                       </span>
                        @endif


                        <input type="submit" id="btn_send" value="ОТПРАВИТЬ">
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
