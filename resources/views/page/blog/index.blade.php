@extends('layouts.app')
@section('content')


    <div class="container-fluid page_title" style="background-image: url('image/ind_2.png')!important;">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h5 class="page_tittle" >Блог</h5>
                    <div class="bread_crumb text-lg-left">
                        <a href="{{route('index')}}">Главная<i class="fa fa-angle-right pl-2" aria-hidden="true"></i></a>
                        <span class="activeColor">Блог</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br>
    <br>
    <br>
    <div class="container">
        <div class="row" >
            @foreach($blogItems as $blogItem)
            <div class="col-12 col-sm-8 col-md-6 col-lg-4 " onclick="location.href='{{route('blogid',$blogItem->id)}}'">
                <div class="card">
                    <img class="card-img" src="{{asset($blogItem->avatar)}}" alt="{{$blogItem->name}}">

                    <div class="card-body">
                        <h4 class="card-title">{{$blogItem->name}}</h4>

                        <p class="card-text">  {{ mb_strlen( strip_tags($blogItem->info, '<b><i><u>') ) > 300 ? mb_substr(strip_tags($blogItem->info, '<b><i><u>'), 0, 300) . ' ...' : strip_tags($blogItem->info, '<b><i><u>') }}
                        </p>
                        <a href="#" class="btn btn-info" >Читать далее</a>
                    </div>
                    <div class="card-footer text-muted d-flex justify-content-between bg-transparent border-top-0">
                        <div class="views">{{$blogItem->created_at->format('Y M d H:i')}}
                        </div>
{{--                        <div class="stats">--}}

{{--                            <i class="fa fa-comment"></i> 12--}}
{{--                        </div>--}}

                    </div>
                </div>
            </div>
            @endforeach

        </div>
    </div>
    </div>
@endsection
