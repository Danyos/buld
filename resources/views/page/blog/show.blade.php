@extends('layouts.app')
@section('content')
    <br>
    <br>


    <div class="container">
        <div class="feature-image page_title" style="background-image: url({{asset($blogItems->avatar)}}); background-size:cover; height: 300px"></div>
        <div class="container-fluid article-content">
            <div class="article-header ">

                <h1 class="entry-title">{{$blogItems->name}}</h1>
                <div class="author">
                    <a class="avatar">
                        <img height="80" width="80" src="{{asset($blogItems->user->avatar)}}" style="object-fit:cover;" alt="{{$blogItems->name}}" />
                    </a>
                    <div class="inner-meta">
                        <span>Автор <a class="created-by">{{$blogItems->user->name}}</a></span>
                        <p class="created-at">{{$blogItems->created_at->format(' M d Y H:i')}}</p>
                    </div>
                </div>
            </div>
            <div class="article-text-wrap">
                <div class="entry-summary">{{$blogItems->auth_offer}}</div>

            {!! $blogItems->info !!}
             </div>
            <div class="container-fluid bg-light main-window">
                <div class="container main-video-window mvw-1">

                    <div class="mvw-text">

                            <iframe class="main-video-frame" src="{{$blogItems->url}}" frameborder="0"
                                    allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                    allowfullscreen></iframe>


                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
