@extends('layouts.app')
@section('content')
    <div class="container-fluid page_title">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h5 class="page_tittle">Наши услуги</h5>
                    <div class="bread_crumb text-lg-left">
                        <a href="{{route('index')}}">Главная<i class="fa fa-angle-right pl-2" aria-hidden="true"></i></a>
                        <span class="activeColor">Наши услуги</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <section id="Services" class="container">

        <div class="col-md-12 d-lg-none section-title text-center">

            <div class="services-inner text-center navbar-nav cat_list">

                @foreach($service as $k=>$services)
                    @if($services->id==$id)
                        <li class="nav-item"><a href="{{route('service',$services->id)}}" class="inActive">{{$services->title}}</a></li>

                    @else
                        <li class="nav-item"><a href="{{route('service',$services->id)}}" class="">{{$services->title}}</a></li>
                    @endif


                @endforeach

            </div><!--services-inner-->

    </section>
    <div class="container-fluid product_page">
        <div class="container">
            <div class="row">
                <div class="col-xl-4 col-md-12 col-lg-4 order-1 order-lg-0">
                    <div class="p_page_list_window">
                        <ul class="navbar-nav cat_list">
                            @foreach($service as $k=>$services)
@if($services->id==$id)
                            <li class="nav-item"><a href="{{route('service',$services->id)}}" class="inActive">{{$services->title}}</a></li>
                                @else
                                    <li class="nav-item"><a href="{{route('service',$services->id)}}" class="">{{$services->title}}</a></li>
    @endif
                            @endforeach
                        </ul>


                    </div>
                </div>
                <div class="col-xl-8 col-md-12 col-lg-8 order-0 order-lg-1">
                    <div class="p_page_product-window">
                        <img src="{{asset($lastservice->avatar)}}" class="img-fluid" alt="{{$lastservice->title}}">
                        <h3 class="services-right-title">    {{$lastservice->title}}</h3>
                        <div class="row">
                            <div class="col-12 services-right-text">
                                <p>
                                    {{$lastservice->description}}
                                </p>

                            </div>
                            <div class="row">
                                @foreach($service_informate as $service_informates)
                                <div class="col-12 col-sm-6">
                                    <div class="wrapper">
                                        <h6>{{$service_informates->title}}</h6>
                                        <p class="m-0 b-l-2">
                                            {{$service_informates->description}}
                                        </p>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                        <hr>





                                    <div>
                                        <p class="text-left">
                                            {{$lastservice->description}}
                                        </p>
                                    </div>
                                    @if($lastservice->url)
                                        <div>

                                            <iframe class="main-video-frame" src="{{$lastservice->url}}" frameborder="0"
                                                    allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                                    allowfullscreen></iframe>

                                        </div>
                                    @endif
                                </div>
                            </div>



            </div>
        </div>
    </div>

@endsection
