<!DOCTYPE html>
<html dir="ltr" lang="en">
<head>
    <title>Sk Ideal</title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <!-- styles -->
    <link rel="stylesheet" href="{{asset('asset/css/bootstrap.min.css')}}">
    <link rel="icon" href="{{asset('asset/image/logo.png')}}">
    <link rel="stylesheet" href="{{asset('asset/css/_icons/css/font-awesome.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('asset/css/_slick/slick.css')}}">
    <link href="{{asset('asset/css/index.css')}}" rel="stylesheet">
    <link href="{{asset('asset/css/media.css')}}" rel="stylesheet">

</head>
<body>
<div class="container-fluid top-panel bg-dark text-light">
    <ul class="top-pList">
        <li><i class="fa fa-volume-control-phone" aria-hidden="true"></i><b>Call: </b>+ (374) 00 00 00 00</li>
        <li><i class="fa fa-map-marker" aria-hidden="true"></i><b>Address: </b>Yerevan, Abovyan 5, 45 home</li>
        <li><i class="fa fa-envelope-o" aria-hidden="true"></i><b>Email: </b>example@gmail.ru</li>
        <li></li>
    </ul>
</div>



<nav class="">
    <input type="checkbox" id="check">
    <label for="check" class="check-response-btn">
        <i class="fa fa-bars"></i>
    </label>
    <label class="logo"><img src="{{asset('asset/image/logo.png')}}"></label>
    <ul>
        <li><a href="{{route('index')}}" class="{{ request()->is('/') || request()->is('/') ? 'active' : '' }}">Главная</a></li>
        <li><a href="{{route('about')}}" class="{{ request()->is('/aboutUs') || request()->is('aboutUs') ? 'active' : '' }}">о нас</a></li>

        <li><a href="{{route('service')}}" class="{{ request()->is('/services*') || request()->is('services*') ? 'active' : '' }}">Наши услуги</a></li>
        <li><a href="{{route('blognews')}}" class="{{ request()->is('/blog/news') || request()->is('blog/news') ? 'active' : '' || request()->is('/blog/*') || request()->is('blog/*') ? 'active' : '' }}">Блог</a></li>

        <li><a href="{{route('contact')}}" class="{{ request()->is('/contact') || request()->is('contact') ? 'active' : '' }}">Контакты</a></li>
    </ul>
</nav>

@yield('content')

<footer class="bg-dark text-light text-center text-md-left">
    <div class="container">
        <div class="row pt-4">
            <div class="col-sm-6 col-md-3 pb-4">
                <h4 class="mb-4">
                    Наши услуги
                </h4>
                <ul class="list-unstyled">

                    @foreach($service->take(4) as $footerservice)
                    <li>
                        <a href="{{route('service',$footerservice->id)}}" class="text-light">{{$footerservice->title}}</a>
                    </li>
                    @endforeach
                </ul>
            </div>
            <div class="col-sm-6 col-md-3 pb-4">
                <h4 class="mb-4">
                    Справочник
                </h4>
                <ul class="list-unstyled">
                    <li>
                        <a href="{{route('about')}}" class="text-light">о нас</a>
                    </li>
                    <li>
                        <a href="{{route('service')}}" class="text-light">Наши услуги</a>
                    </li>
                    <li>
                        <a href="{{route('blognews')}}" class="text-light">Блог</a>
                    </li><li>
                        <a href="{{route('contact')}}" class="text-light">Контакты</a>
                    </li>
                </ul>
            </div>
            <div class="col-md-6 pb-4">
                <h4 class="mb-4">
                    Название компании:
                </h4>
                <p>
                    Многие думают, что Lorem Ipsum - взятый с потолка псевдо-латинский набор слов, но это не совсем так. Его корни уходят в один фрагмент классической латыни 45 года н.э., то есть более двух тысячелетий назад. Ричард МакКлинток, профессор латыни из колледжа Hampden-Sydney, штат Ви
                </p>
            </div>
        </div>
        <div class="col d-flex justify-content-center mb-4">

            <a href="#" class="d-block px-3"><i class="fa fa-facebook" style="font-size: 30px; color:white;" aria-hidden="true"></i></a>
            <a href="#" class="d-block px-3"><i class="fa fa-instagram" style="font-size: 30px; color:white;" aria-hidden="true"></i></a>
            <a href="#" class="d-block px-3"><i class="fa fa-telegram" style="font-size: 30px; color:white;" aria-hidden="true"></i></a>
        </div>
        <p class="text-center text-secondary border-top border-secondary py-4">
            Company Name © 2020
        </p>
    </div>

</footer>
<script src="{{asset('asset/JS/_JQuery/jquery.js')}}"></script>
@if ($errors->any())
    <script>
        $(window).scrollTop($('#myLocation').offset().top)
    </script>
@endif
@if(session()->has('succsess'))
    <script>
        $(window).scrollTop($('#myLocation').offset().top)
    </script>
@endif
<script src="{{asset('asset/JS/_JQuery/ajax.jquery.min.js')}}" type="text/javascript"></script>
<script src="{{asset('asset/JS/_Popper/popper.js')}}"></script>
<script src="{{asset('asset/JS/bootstrap.min.js')}}"></script>
<script src="{{asset('asset/JS/_Slick/slick.js')}}"></script>
<script src="{{asset('asset/JS/my_slick.js')}}"></script>
</body>
</html>
